#!/bin/bash

export X=20
export Y=20
export Z=2000
export OUTDIR=tmp_single

export N="$((X*Y*Z))"

TIME_AA="`date +%s`"
while read line; do
    echo ">>> " $line
    TIME_A="`date +%s`"
    PRNT="`eval $line`"
    TIME_B="`date +%s`"
    echo "[" $((TIME_B-TIME_A)) "]" $line "-->" $? ":" $PRNT 
done < $1 # yes, this is insecure. NEVER EVER call this script with suid/sgid
TIME_BB="`date +%s`"
echo "[" $((TIME_BB-TIME_AA)) "] -- total script time --"
