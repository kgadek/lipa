#!/bin/bash

# SETTINGS
N=800
M=100
O=10
OUTPUTDIR_SINGLE=tmp_bench
OUTPUTDIR_MULTI=tmp_bench2
LOGFILE="`pwd`/fs_bench_`uname -n`.log"

# UTILS
write_log() {
    echo -n "[" "$1" "] " | tee -a $LOGFILE
    shift
    echo $@ | tee -a $LOGFILE
}

rm -rf $OUTPUTDIR_SINGLE
rm -rf $OUTPUTDIR_MULTI

# BENCHMARK

N_ORIG=$N
N=$((N*M*O))
mkdir -p $OUTPUTDIR_SINGLE
cd $OUTPUTDIR_SINGLE

TIME_B0="`date +%H%M%S`"
write_log $TIME_B0 "====================[ N:" $N "]===================="
write_log $TIME_B0 "Benchmark: create empty files..."

for ((i=0;$i<$N;i++)); do
    touch $i;
done

TIME_B1="`date +%H%M%S`"
write_log $TIME_B1 ".    Done in" $((TIME_B1 - TIME_B0)) "second(s)"
write_log $TIME_B1 "Benchmark: directory size"

DIRSIZE1="`du -hs . | awk '{print $1}'`"

TIME_B2="`date +%H%M%S`"
write_log $TIME_B2 ".    Size:" $DIRSIZE1
write_log $TIME_B2 ".    Done in" $((TIME_B2 - TIME_B1)) "second(s)"
write_log $TIME_B2 "Benchmark: fill files with content (12 bytes)"

for ((i=0;$i<$N;i++)); do
    echo "ala ma kota" > $i;
done

TIME_B3="`date +%H%M%S`"
write_log $TIME_B3 ".    Done in" $((TIME_B3 - TIME_B2)) "second(s)"
write_log $TIME_B3 "Benchmark: list directory: ls -Al"

ls -Al > /dev/null

TIME_B4="`date +%H%M%S`"
write_log $TIME_B4 ".    Done in" $((TIME_B4 - TIME_B3)) "second(s)"
write_log $TIME_B4 "Benchmark: directory size"

DIRSIZE2="`du -hs . | awk '{print $1}'`"

TIME_B5="`date +%H%M%S`"
write_log $TIME_B5 ".    Size:" $DIRSIZE2
write_log $TIME_B5 ".    Done in" $((TIME_B5 - TIME_B4)) "second(s)"
write_log $TIME_B5 "Benchmark: random access (5. times)"

cat 0 > /dev/null
cat $((N - 1)) > /dev/null
cat $((N/3)) > /dev/null
cat $((2*N/3)) > /dev/null
cat $((N/2)) > /dev/null

TIME_B6="`date +%H%M%S`"
write_log $TIME_B6 ".    Done in" $((TIME_B6 - TIME_B5)) "second(s)"
write_log $TIME_B6 "Benchmark: remove all files"

rm -rf *

TIME_B7="`date +%H%M%S`"
write_log $TIME_B6 ".    Done in" $((TIME_B7 - TIME_B6)) "second(s)"


write_log $TIME_B5 "____________________[S:" $((TIME_B7 - TIME_B0)) "sec. ]____________________"


cd ..
rm -rf $OUTPUTDIR_SINGLE
mkdir -p $OUTPUTDIR_MULTI
cd $OUTPUTDIR_MULTI
N=$N_ORIG


TIME_B0="`date +%H%M%S`"
write_log $TIME_B0 "====================[ multi (N,M,O):" $N $M $O "]===================="
write_log $TIME_B0 "Benchmark: create empty files..."

for ((i=0;$i<$N;i++)); do
    for ((j=0;$j<$M;j++)); do
	mkdir -p $i/$j
	for ((k=0;$k<$O;k++)); do
	    touch $i/$j/$k
	done
    done
done

TIME_B1="`date +%H%M%S`"
write_log $TIME_B1 ".    Done in" $((TIME_B1 - TIME_B0)) "second(s)"
write_log $TIME_B1 "Benchmark: directory size"

DIRSIZE1="`du -hs . | awk '{print $1}'`"

TIME_B2="`date +%H%M%S`"
write_log $TIME_B2 ".    Size:" $DIRSIZE1
write_log $TIME_B2 ".    Done in" $((TIME_B2 - TIME_B1)) "second(s)"
write_log $TIME_B2 "Benchmark: fill files with content (12 bytes)"

for ((i=0;$i<$N;i++)); do
    for ((j=0;$j<$M;j++)); do
	mkdir -p $i/$j
	for ((k=0;$k<$O;k++)); do
    	    echo "ala ma kota" > $i/$j/$k
	done
    done
done

TIME_B3="`date +%H%M%S`"
write_log $TIME_B3 ".    Done in" $((TIME_B3 - TIME_B2)) "second(s)"
write_log $TIME_B3 "Benchmark: list directory: ls -Al"

ls -Al > /dev/null

TIME_B4="`date +%H%M%S`"
write_log $TIME_B4 ".    Done in" $((TIME_B4 - TIME_B3)) "second(s)"
write_log $TIME_B4 "Benchmark: directory size"

DIRSIZE2="`du -hs . | awk '{print $1}'`"

TIME_B5="`date +%H%M%S`"
write_log $TIME_B5 ".    Size:" $DIRSIZE2
write_log $TIME_B5 ".    Done in" $((TIME_B5 - TIME_B4)) "second(s)"
write_log $TIME_B5 "Benchmark: random access (5. times)"

cat 0/0/0 > /dev/null
cat $((N-1))/$((M-1))/$((O-1)) > /dev/null
cat $((N/3))/$((M/3))/$((O/3)) > /dev/null
cat $((2*N/3))/$((2*M/3))/$((2*O/3)) > /dev/null
cat $((N/2))/$((M/2))/$((O/2)) > /dev/null

TIME_B6="`date +%H%M%S`"
write_log $TIME_B6 ".    Done in" $((TIME_B6 - TIME_B5)) "second(s)"
write_log $TIME_B6 "Benchmark: remove all files"

rm -rf *

TIME_B7="`date +%H%M%S`"
write_log $TIME_B6 ".    Done in" $((TIME_B7 - TIME_B6)) "second(s)"
write_log $TIME_B5 "____________________[M:" $((TIME_B7 - TIME_B0)) "sec. ]____________________"


cd ..
rm -rf $OUTPUTDIR_MULTI