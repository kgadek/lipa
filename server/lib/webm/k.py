#!/usr/bin/env python

import string as sr
import subprocess as s
import json
import re
import os
import uuid
import shutil as sh
import sys


def mainOffline(name, addr, files, mailGpg, clientid, login, password, serveraddr):
	tmpname = str(uuid.uuid4())
	if createSettingsConfOffline(name, addr) == False:
		return False
	files.append("settings.conf")
	if createPackageSignedEncrypted(files, tmpname+".tgx.gpg", mailGpg) != 0:
		print "Error: tworzenie paczki %s sie nie powiodlo\n" % tmpname
		return False
	sndRet = sendFileViaPost(login, password, tmpname+".tgx.gpg", serveraddr, clientid, tmpname+".header")
	if sndRet != 201:
		print "Error: serwer zwrocil kod bledu %s\n" % str(sndRet)
	else:
		print "OK"
	return tmpname

def main(name, addr, files, mailGpg, clientid, login, password, serveraddr):
	tmpname = str(uuid.uuid4())
	if createSettingsConf(name, addr) == False:
		return False
	files.append("settings.conf")
	if createPackageSignedEncrypted(files, tmpname+".tgx.gpg", mailGpg) != 0:
		print "Error: tworzenie paczki %s sie nie powiodlo\n" % tmpname
		return False
	sndRet = sendFileViaPost(login, password, tmpname+".tgx.gpg", serveraddr, clientid, tmpname+".header")
	if sndRet != 201:
		print "Error: serwer zwrocil kod bledu %s\n" % str(sndRet)
	else:
		print "OK"
	return tmpname

def createSettingsConfOffline(name, addr):
	(lat, lng) = (50.0, 50.0)
	with open('settings.conf', 'w') as f:
		f.write(
				'[Client]\n'
				'Name=%s\n'
				'Address=%s\n'
				'Lat=%s\n'
				'Lng=%s\n' % (name, addr, lat, lng)
				)
	return True

def createSettingsConf(name, addr):
	latlng = translateAddrToCoords(addr)
	if latlng == False:
		return False
	(lat, lng) = latlng
	with open('settings.conf', 'w') as f:
		f.write(
				'[Client]\n'
				'Name=%s\n'
				'Address=%s\n'
				'Lat=%s\n'
				'Lng=%s\n' % (name, addr, lat, lng)
				)
	return True

def translateAddrToCoords(addr):
	try:
		addr = re.sub(r'\s+',r'+',addr)
		reply = s.check_output([
			'curl',
			'http://maps.googleapis.com/maps/api/geocode/json?address=%s&sensor=false' % (addr ,)
			])
		reply = json.loads(reply)
		if reply['status'] != 'OK': return False
		reply = reply['results'][0]['geometry']['location']
		lat = reply['lat']
		lng = reply['lng']
		return (lat, lng)
	except Exception:
		return False

def createPackageSignedEncrypted(files, outfile, receiverMail):
	try:
		return s.call(
				'tar cJO %s | gpg --output "%s" --encrypt --sign --recipient "%s" -' % (' '.join(files), outfile, receiverMail),
				shell=True)
	except Exception:
		return False

def sendFileViaPost(login, password,filename,serveraddr,clientid,tmpFileForLogin):
	try:
		Cmd = 'curl -s --write-out "%%{http_code}" -D "%s" -u "%s:%s" --data-binary "@%s" "%s/clients/%s"' % (tmpFileForLogin, login, password, filename, serveraddr, clientid)
		return sr.atoi(s.check_output(Cmd, shell=True))
	except Exception as e:
		return False


if __name__ == '__main__':
	if len(sys.argv) < 7:
		print "Uzycie: %s nazwa_klienta_systemu login haslo nazwa_odbiorcy adres_docelowy plik1 [plik2...]\n" % sys.argv[0]
	else:
		serveraddr = "localhost:8000"
		mailGpg    = "serwer@lipa"
		clientid = sys.argv[1]
		login    = sys.argv[2]
		password = sys.argv[3]
		name     = sys.argv[4]
		addr     = sys.argv[5]
		files    = sys.argv[6:]
		uuid = mainOffline(name, addr, files, mailGpg, clientid, login, password, serveraddr)
		try: os.remove(uuid+".tgx.gpg")
		except Exception: pass
		try: os.remove(uuid+".header")
		except Exception: pass
		try: os.remove(uuid+".tgx")
		except Exception: pass
