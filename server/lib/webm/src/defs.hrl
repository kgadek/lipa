%% @doc Rekord klienta. Zawiera jedynie podstawowe informacje pozwalające na komunikację przez HTTP i na identyfikację klienta.
-record(client, {
        id    :: string(),    % id, czyli string o postaci "klien01", ...
        login :: string(),    % login do autentykacji HTTP
        pass  :: string() }). % hasło do autentykacji HTTP

%% @doc Rekord węzła końcowego. Zawiera informacje niezbędne do właściwego wyboru miejsca drukowania.
-record(node, {
        id     :: string(),    % id, czyli string o postaci "node01", ...
        login  :: string(),    % login do autentykacji HTTP
        pass   :: string(),    % hasło do autentykacji HTTP
        lat    :: float(),     % położenie node'a: szerokość geograficzna (-90..+90, S<N)
        lng    :: float(),     % położenie node'a: długość geograficzna (-180..+180, W<E)
        active :: boolean()}). % czy node jest aktywny. Pole w przyszłości będzie pozwalało na dynamiczne zarządzanie
                               % zamontowanymi node'ami

%% @doc Rekord opisujący stan błędu.
-record(error, {
        descr  = "" :: string()}).

%% @doc Rekord opisujący zadanie drukowania.
-record(job, {
        id      :: string(),     % id; powstaje w wyniku działania now/0, które
                                 %      * gwarantuje nam unikalność
                                 %      * podaje przybliżoną godzinę (milisekundy od 1 stycznia 1970)
        client  :: string(),     % id klienta zlecającego zadanie
        dst_lat :: float(),      % geokodowane położenie adresu docelowego: szerokość geograficzna (-90..+90, S<N)
        dst_lng :: float(),      % geokodowane położenie adresu docelowego: długość geograficzna (-180..+180, W<E)
        status  :: 'waiting'     % stan: oczekuje na wydruk
                |  'printing'    % stan: w trakcie drukowania
                |  'printed'     % stan: wydrukowano
                |  'cancelled'   % stan: anulowano
                |  'disposed'    % stan: anulowano a paczka została usunięta z dysku
                |  'cleared'     % stan: paczka została usunięta z dysku w normalnym trybie
                |  #error{},
        nodes   :: [string()]    % Lista node'ów, przez które przeszło zadanie. Głowa
                                 % tej listy to node, któremu obecnie zlecono zadanie. Służy do przenoszenia zadań między node'ami
        }).

