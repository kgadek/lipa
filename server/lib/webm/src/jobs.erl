%% vim: syntax=erlang shiftwidth=4 tabstop=4 expandtab

-module(jobs).

-export([
    init/1,
    content_types_accepted/2,
    content_types_provided/2,
    allowed_methods/2,
    is_authorized/2,
    post_is_create/2,
    create_path/2,
    resource_exists/2,
    delete_resource/2,
    process_post/2,
    from_urlencoded/2,
    to_text/2,

    reload_dispatch/0,
    show_table/1
    ]).

-import(math, [pi/0, sin/1, cos/1, acos/1]).
-include("defs.hrl").
-include_lib("stdlib/include/qlc.hrl").
-include_lib("webmachine/include/webmachine.hrl").

-record(ctx, {who    :: 'node' | 'client',
              node   :: integer(), % node.id
              client :: integer(), % client.id
              job    :: #job{}}).

init(Opts) ->
    {who, Who} = proplists:lookup(who, Opts),
    {{trace, "/tmp"}, #ctx{who = Who}}.

content_types_accepted(RD, Ctx) ->
    {[{"application/x-www-form-urlencoded", from_urlencoded}], RD, Ctx}.

content_types_provided(RD, Ctx) ->
    {[{"text_plain", to_text}], RD, Ctx}.

allowed_methods(RD, Ctx) ->
    {['GET', 'POST', 'PUT', 'DELETE'], RD, Ctx}.

is_authorized(RD, #ctx{who = node} = Ctx) ->
    case wrq:get_req_header("authorization", RD) of
        "Basic "++Base64 ->
            NId = wrq:path_info(nodeid, RD),
            Str = base64:mime_decode_to_string(Base64),
            [User, Pass] = string:tokens(Str, ":"),
            F = fun() -> mnesia:select(node, [{#node{id='$1', login=User, pass=Pass, _='_'}, [{'=:=', '$1', NId}], ['$1']}]) end,
            case mnesia:transaction(F) of
                {atomic, [Node]} ->
                    {true, RD, Ctx#ctx{node = Node}};
                _ ->
                    {"Basic realm=Lipa", RD, Ctx}
            end;
        _ ->
            {"Basic realm=Lipa", RD, Ctx}
    end;
is_authorized(RD, #ctx{who = client} = Ctx) ->
    case wrq:get_req_header("authorization", RD) of
        "Basic "++Base64 ->
            NId = wrq:path_info(clientid, RD),
            Str = base64:mime_decode_to_string(Base64),
            [User, Pass] = string:tokens(Str, ":"),
            F = fun() -> mnesia:select(client, [{#client{id='$1', login=User, pass=Pass, _='_'}, [{'=:=', '$1', NId}], ['$1']}]) end,
            case mnesia:transaction(F) of
                {atomic, [Client]} ->
                    {true, RD, Ctx#ctx{client = Client}};
                _ ->
                    {"Basic realm=Lipa", RD, Ctx}
            end;
        _ ->
            {"Basic realm=Lipa", RD, Ctx}
    end.

post_is_create(RD, #ctx{who = client} = Ctx) ->
    {true, RD, Ctx};
post_is_create(RD, #ctx{who = node} = Ctx) ->
    {false, RD, Ctx}.

create_path(RD, #ctx{who = client} = Ctx) ->
    Now = lists:flatten(string:join([integer_to_list(X) || X<-tuple_to_list(now())],"_")),
    Job = #job{id      = Now,
               client  = Ctx#ctx.client,
               status  = waiting,
               nodes   = []},
    {Now, RD, Ctx#ctx{job = Job}}.

resource_exists(RD, #ctx{who = client} = Ctx) ->
    case wrq:method(RD) of
        M when M =:= 'PUT';
               M =:= 'DELETE' ->
            JobId = wrq:path_info(jobid, RD),
            S = fun() -> qlc:eval(qlc:q([X || X <- mnesia:table(job),
                                              X#job.id =:= JobId
                                             ])) end,
            case mnesia:transaction(S) of
                {atomic, [Job]} ->
                    {true, RD, Ctx#ctx{job = Job}};
                _ ->
                    {false, RD, Ctx}
            end;
        M when M =:= 'POST';
               M =:= 'GET' ->
            {true, RD, Ctx}
    end;
resource_exists(RD, Ctx) ->
    {true, RD, Ctx}.

delete_resource(RD, #ctx{who = client, job = Job} = Ctx) ->
    U = fun() ->
            [J] = qlc:eval(qlc:q([X || X <- mnesia:table(job),
                                       X#job.id =:= Job#job.id
                                      ])),
            case J#job.status of
                waiting -> mnesia:write(J#job{status = cancelled}), cancelled;
                _       -> too_late
            end
    end,
    case mnesia:transaction(U) of
        {atomic, cancelled} -> {true, RD, Ctx};
        _                   -> {{halt, 409}, RD, Ctx}
    end.

process_post(RD, #ctx{who = node} = Ctx) ->
    {true, RD, Ctx}.

from_urlencoded(RD, #ctx{who = client, job = Job} = Ctx) ->
    Body = wrq:req_body(RD),
    case wrq:method(RD) of
        'DELETE' -> ok;
        'PUT' ->
            <<"delete">> = Body,
            delete_resource(RD, Ctx);
        'POST' ->
            Pkgname = [Ctx#ctx.client, "__", Ctx#ctx.job#job.id, ".tgx"],
            Pkgpath = ["priv/pkgs/", Pkgname],
            Filename = [Pkgpath, ".gpg"],
            ok = file:write_file(Filename, Body),
            case verify_incoming_package(Pkgpath, Filename) of
                {Lat, Lng} when is_float(Lat), is_float(Lng) ->
                    spawn(fun() -> select_and_send({Lat,Lng},Job,Pkgpath,Pkgname) end),
                    {true, RD, Ctx};
                _ ->
                    file:delete(Filename),
                    file:delete(Pkgpath),
                    {{halt, 400}, RD, Ctx}
            end
    end;
from_urlencoded(RD, #ctx{who = node} = Ctx) ->
    case wrq:method(RD) of
        'POST' ->
            Rsp = wrq:set_resp_body("POST n", RD),
            {true, Rsp, Ctx};
        'PUT' ->
            Rsp = wrq:set_resp_body("PUT n", RD),
            {true, Rsp, Ctx}
    end.

to_text(RD, #ctx{who = client} = Ctx) ->
    case string:tokens(wrq:path(RD), "/") of
        ["clients", ClientID, JobID] ->
            F = fun() ->
                    qlc:eval(qlc:q([[Id ++ " : " ++ atom_to_list(Status) ++ "\n"]
                                    || #job{client=CID, id=Id, status=Status} <- mnesia:table(job),
                                       Id =:= JobID,
                                       CID =:= ClientID]))
            end,
            QRes = case mnesia:transaction(F) of
                {aborted, _Reason} -> [];
                {atomic, L}        -> L
            end,
            {QRes, RD, Ctx};
        ["clients", ClientID] ->
            Body = [
                    "Dostępne zlecenia dla ", ClientID, ":\n"
                    "=========================================\n"
                   ],
            F = fun() ->
                    qlc:eval(qlc:q([[" * " ++ Id ++ " : " ++ atom_to_list(Status) ++ "\n"]
                                    || #job{client=CID, id=Id, status=Status} <- mnesia:table(job),
                                       CID =:= ClientID]))
            end,
            QRes = case mnesia:transaction(F) of
                {aborted, _Reason} -> [];
                {atomic, L}        -> L
            end,
            {Body ++ QRes, RD, Ctx}
    end;
to_text(RD, #ctx{who = node} = Ctx) ->
    case string:tokens(wrq:path(RD), "/") of
        ["nodes", _Nodeid, _Jobid] ->
            {"GET nj", RD, Ctx}
    end.

verify_incoming_package(Pkgname, Filename) ->
    GpgCmd = (["gpg --output \"", Pkgname, "\" --decrypt \"", Filename, "\""]),
    case cmd_return_status(GpgCmd) of
        0 ->
            os:cmd(["rm -f \"", Filename, "\""]),
            Config = parse_ini(Pkgname),
            try begin
                    {_, [_|_]       } = lists:keyfind({"Client", "Name"    }, 1, Config),
                    {_, [_|_]       } = lists:keyfind({"Client", "Address" }, 1, Config),
                    {_, [_|_] = Lat } = lists:keyfind({"Client", "Lat"     }, 1, Config),
                    {_, [_|_] = Lng } = lists:keyfind({"Client", "Lng"     }, 1, Config),
                    {list_to_float(Lat), list_to_float(Lng)}
                end
            catch
                _:_ -> false
            end;
        _ -> false
    end.

select_and_send({Lat,Lng}, Job, Pkgpath, Pkgname) ->
    #job{nodes = NodesHistory} = Job,
    F = fun() -> qlc:eval(qlc:q(
                    [{{X#node.lat, X#node.lng}, X#node.id}
                     || X <- mnesia:table(node),
                        X#node.active =:= true,
                        not contains(X#node.id, NodesHistory)]
                    )) end,
    LLToDist = fun({Lt,Lg}) -> calculate({Lat,Lng}, {Lt,Lg}) end,
    case mnesia:transaction(F) of
        {atomic, []} ->
            io:format("Nie ma innych nołdów...~n");
        {atomic, AllNodes} ->
            WithDist = lists:keymap(LLToDist, 1, AllNodes),
            {_Dist,BestId} = lists:min(WithDist),
            io:format("Best is ~p~n", [BestId]),
            Ins = fun() ->
                    mnesia:write(Job#job{nodes  = [BestId|NodesHistory],
                                        dst_lat = Lat,
                                        dst_lng = Lng}),
                    {ok, _} = file:copy(Pkgpath, ["/mnt/lipa/", BestId, "/lipa/packages/", Pkgname])
            end,
            {atomic, _} = mnesia:transaction(Ins);
        {aborted, R} ->
            io:format("KUPA!~p~n", [R])
    end.

%% Helper

contains(_X,[]) -> false;
contains(X,[H|_]) when X=:=H -> true;
contains(X,[_|T]) -> contains(X,T).

-spec calculate({float(), float()}, {float(), float()}) -> float().
%% @doc Oblicza odległość współrzędnych geograficznych między sobą.
%% Wynikiem jest kąt w radianach.
calculate( {GpsThA, GpsPhA}, {GpsThB, GpsPhB}) ->
    Coef = pi()/180.0,
    {ThA, PhA} = {GpsThA*Coef, GpsPhA*Coef},
    {ThB, PhB} = {GpsThB*Coef, GpsPhB*Coef},
    {XA,YA,ZA} = {cos(ThA)*cos(PhA), cos(ThA)*sin(PhA), sin(ThA)},
    {XB,YB,ZB} = {cos(ThB)*cos(PhB), cos(ThB)*sin(PhB), sin(ThB)},
    case XA*XB+YA*YB+ZA*ZB of
        X when X > 1.0  -> acos(1.0);
        X when X < -1.0 -> acos(-1.0);
        X               -> acos(X)
    end.

parse_ini(Filename) ->
    Str = string:tokens(os:cmd(["tar JxOf \"", Filename, "\" settings.conf"]), "\n"),
    parse_ini(Str, default, []).
parse_ini([], _Group, Res) ->
    Res;
parse_ini([Head|Tail], Group, Res) ->
    [F|R] = Str = string:strip(Head),
    case F of
        $# -> parse_ini(Tail, Group, Res);
        $[ -> parse_ini(Tail, string:strip(R, both, $]), Res);
        _ ->
            [K|V] = string:tokens(Str, "="),
            parse_ini(Tail, Group, [{{Group, K}, string:join(V,"=")}|Res])
    end.

%% @doc Run command and only get return status.
cmd_return_status(CmdIO) ->
    Cmd = lists:flatten(CmdIO),
    Port = erlang:open_port({spawn, Cmd}, [{line, 100}, exit_status, stderr_to_stdout]),
    AuxFun = fun(Self, Stat) ->
            receive
                {Port, {data, _}}                 -> Self(Self, Stat);
                {Port, {exit_status, ReturnCode}} -> ReturnCode
            after 2000 -> Stat
            end
    end,
    AuxFun(AuxFun, false).

reload_dispatch() ->
    {ok, D} = file:consult("priv/dispatch.conf"),
    application:set_env(webmachine, dispatch_list, D).

%% Administrate
show_table(Table) when is_atom(Table) ->
    F = fun() ->
            qlc:eval(qlc:q([X || X <- mnesia:table(Table)]))
    end,
    mnesia:transaction(F).

