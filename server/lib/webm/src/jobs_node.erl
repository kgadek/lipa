%% vim: syntax=erlang shiftwidth=4 tabstop=4 expandtab

-module(jobs_node).

-include("defs.hrl").

-export([init/1,
         content_types_provided/2,
         content_types_accepted/2,
         allowed_methods/2,
         to_text/2,
         from_post/2,
         post_is_create/2,
         create_path/2,
         is_authorized/2,
         is_conflict/2
        ]).

-include_lib("webmachine/include/webmachine.hrl").
-include_lib("stdlib/include/qlc.hrl").
-record(ctx, {
              status :: atom(),
              node   :: #node{},
              job    :: #job{}
             }).


-spec init([]) -> {ok, #ctx{}}.
init(ConfigProps) ->
    {status, Op} = proplists:lookup(status, ConfigProps),
    {{trace, "/tmp"}, #ctx{status = Op}}.

-spec allowed_methods(term(), #ctx{}) -> {[atom()], term(), #ctx{}}.
allowed_methods(ReqData, Context) ->
    {['GET', 'PUT', 'POST'], ReqData, Context}.

-spec content_types_provided(term(), #ctx{}) -> {[term()], term(), #ctx{}}.
content_types_provided(ReqData, Context) ->
    {[{"text/plain", to_text}], ReqData, Context}.

-spec content_types_accepted(term(), #ctx{}) -> {[term()], term(), #ctx{}}.
content_types_accepted(ReqData, Context) ->
    {[{"application/x-www-form-urlencoded", from_post}], ReqData, Context}.

-spec is_authorized(term(), #ctx{}) -> {'true' | string(), term(), #ctx{}}.
%% @doc Dokonuje weryfikacji użytkownika.
%% Weryfikacja jest dwupoziomowa. Pierwszy poziom to prosty system
%% login/hasło przez HTTP. Nie gwarantuje bezpieczeństwa danych, jednak
%% skuteczny przeciwko głupim dowcipom.
%% Drugi etap jest bardziej mozolny, polega na odebraniu paczki. Paczka
%% jest zaszyfrowana kluczem publicznym serwera i podpisana kluczem
%% prywatnym klienta.
is_authorized(ReqData, Context) ->
    case wrq:get_req_header("authorization", ReqData) of
        "Basic "++Base64 ->
            Str = base64:mime_decode_to_string(Base64),
            [User, Pass] = string:tokens(Str, ":"),
            case mnesia:transaction(fun() -> qlc:eval(
                                qlc:q( [X ||
                                        X<-mnesia:table(node),
                                        X#node.id =:= wrq:path_info(nodeid, ReqData),
                                        X#node.login =:= User,
                                        X#node.pass =:= Pass]
                                     )) end) of 
                {atomic, [Node]} ->
                    {true, ReqData, Context#ctx{node = Node}};
                _ ->
                    {"Basic realm=Lipa", ReqData, Context}
            end;
        _ ->
            {"Basic realm=Lipa", ReqData, Context}
    end.


-spec to_text(term(), #ctx{}) -> {[term()], term(), #ctx{}}.
%% @doc Helper. Nie ma praktycznego użytku oprócz takiego, że serwer nie krzyczy o błędach
%% gdy się poprosi GET-em.
to_text(ReqData, Context) ->
    {"", ReqData, Context}.

-spec post_is_create(term(), #ctx{}) -> {boolean(), term(), #ctx{}}.
post_is_create(ReqData, Context) ->
    {true, ReqData, Context}.

-spec create_path(term(), #ctx{}) -> {string(), term(), #ctx{}}.
%% @doc Gdy prawdziwy jest test post_is_create/2, to dla POST-ów wywoływana jest ta
%% metoda; generuje ona ścieżkę (URI) a potem wszystko odbywa się tak, jak z PUT-em.
create_path(ReqData, Context) ->
    {"printing", ReqData, Context}.

-spec from_post(term(), #ctx{}) -> {'true', term(), #ctx{}}.
%% @doc Odbieranie danych (POST ale także PUT).
from_post(ReqData, Context) ->
    {true, ReqData, Context}.

-spec is_conflict(term(), #ctx{}) -> {boolean(), term(), #ctx{}}.
%% @doc Przy pomocy kodów HTML stwierdza, czy zmiana stanu była możliwa (a jeśli 
%% tak to dokonano jej atomicznie razem ze sprawdzeniem tego faktu).
%% Jest kilka przypadków w zależności, czy chcemy zacząć drukować, oświadczyć, że
%% wydrukowaliśmy czy że nastąpił błąd.
is_conflict(ReqData, #ctx{status = printing} = Context) ->
    JobId = wrq:path_info(jobid, ReqData),
    case mnesia:transaction(fun() ->
                    [J] = mnesia:wread({job, JobId}),
                    case J#job.status of
                        waiting ->
                            mnesia:write(J#job{status = printing});
                        _ ->
                            mnesia:abort("Conflict")
                    end
            end) of
        {atomic, _} -> {false, ReqData, Context};
        _           -> {true, ReqData, Context}
    end;
is_conflict(ReqData, #ctx{status = printed} = Context) ->
    JobId = wrq:path_info(jobid, ReqData),
    {atomic, _} = mnesia:transaction(fun() ->
                    [J] = mnesia:wread({job, JobId}),
                    mnesia:write(J#job{status = printed})
            end),
    {false, ReqData, Context};
is_conflict(ReqData, #ctx{status = rejected} = Context) ->
    JobId = wrq:path_info(jobid, ReqData),
    mnesia:transaction(
        fun() ->
                [J] = mnesia:wread({job, JobId}),
                case J#job.status of
                    cancelled -> ok;
                    disposed -> ok;
                    cleared -> ok;
                    _ ->
                        #job{client = Client, id = Id, nodes = Nodes, dst_lat = Lat, dst_lng = Lng} = J,
                        Pkgname = [Client, "__", Id, ".tgx"],
                        F = fun({X, Xlat, Xlng}) ->
                                {jobs_client:calculate({Xlat,Xlng}, {Lat, Lng}), X}
                        end,
                        Ns = qlc:eval(qlc:q( [{X, X#node.lat, X#node.lng}
                                              || X <- mnesia:table(node),
                                                 X#node.active =:= true,
                                                 not lists:any(fun(Y) -> Y =:= X#node.id end, Nodes)])),
                        [{_,H}|_] = lists:keysort(1, lists:map(F, Ns)),
                        file:copy(["priv/pkgs/", Pkgname], ["/mnt/lipa/", H, "/", Pkgname]),
                        mnesia:write(
                            J#job{
                                status = waiting,
                                nodes = [H|Nodes]})
                end

        end),
    {false, ReqData, Context};
is_conflict(ReqData, #ctx{status = failed} = Context) ->
    JobId = wrq:path_info(jobid, ReqData),
    {atomic, _} = mnesia:transaction(fun() ->
                    [J] = mnesia:wread({job, JobId}),
                    mnesia:write(J#job{status = #error{}})
            end),
    {false, ReqData, Context}.

