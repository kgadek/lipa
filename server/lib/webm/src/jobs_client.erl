%% vim: syntax=erlang shiftwidth=4 tabstop=4 expandtab

-module(jobs_client).

-import(math, [pi/0, sin/1, cos/1, acos/1]).
-include("defs.hrl").
-include_lib("stdlib/include/qlc.hrl").

-export([init/1,
         content_types_provided/2,
         content_types_accepted/2,
         allowed_methods/2,
         to_text/2,
         calculate/2,
         post_is_create/2, create_path/2,
         is_authorized/2,
         from_file/2
        ]).

-include_lib("webmachine/include/webmachine.hrl").
-record(ctx, {
              operation     :: atom(),
              client        :: #client{},
              job           :: #job{}
             }).


-spec init([]) -> {ok, #ctx{}}.
init(ConfigProps) ->
    {operation, Op} = proplists:lookup(operation, ConfigProps),
    {ok, #ctx{operation = Op}}.


-spec allowed_methods(term(), #ctx{}) -> {[atom()], term(), #ctx{}}.
allowed_methods(ReqData, Context) ->
    {['GET', 'PUT', 'POST'], ReqData, Context}.


-spec content_types_provided(term(), #ctx{}) -> {[term()], term(), #ctx{}}.
content_types_provided(ReqData, Context) ->
    {[{"text/plain", to_text}], ReqData, Context}.


-spec content_types_accepted(term(), #ctx{}) -> {[{string(), atom()}], term(), #ctx{}}.
content_types_accepted(ReqData, Context) ->
    {[{"application/x-www-form-urlencoded", from_file}], ReqData, Context}.

-spec is_authorized(term(), #ctx{}) -> {'true' | string(), term(), #ctx{}}.
%% @doc Dokonuje weryfikacji użytkownika.
%% Weryfikacja jest dwupoziomowa. Pierwszy poziom to prosty system
%% login/hasło przez HTTP. Nie gwarantuje bezpieczeństwa danych, jednak
%% skuteczny przeciwko głupim dowcipom.
%% Drugi etap jest bardziej mozolny, polega na odebraniu paczki. Paczka
%% jest zaszyfrowana kluczem publicznym serwera i podpisana kluczem
%% prywatnym klienta.
is_authorized(ReqData, Context) ->
    case wrq:get_req_header("authorization", ReqData) of
        "Basic "++Base64 ->
            Str = base64:mime_decode_to_string(Base64),
            [User, Pass] = string:tokens(Str, ":"),
            case mnesia:transaction(fun() -> qlc:eval(
                                qlc:q( [X ||
                                        X<-mnesia:table(client),
                                        X#client.id =:= wrq:path_info(clientid, ReqData),
                                        X#client.login =:= User,
                                        X#client.pass =:= Pass]
                                     )) end) of 
                {atomic, [Client]} ->
                    {true, ReqData, Context#ctx{client = Client}};
                _ ->
                    {"Basic realm=Lipa", ReqData, Context}
            end;
        _ ->
            {"Basic realm=Lipa", ReqData, Context}
    end.


-spec state_to_str(#error{} | atom()) -> iolist().
%% @doc Helper: konwersja statusu na coś, co da się wyświetlić.
state_to_str(X) when is_record(X, error) ->
    ["error: ", X#error.descr];
state_to_str(X) when is_atom(X) ->
    atom_to_list(X).

-spec to_text(term(), #ctx{}) -> {[term()], term(), #ctx{}}.
%% @doc Wynik działania GET.
to_text(ReqData, #ctx{client = Client} = Context) ->
    Body = case string:tokens(wrq:path(ReqData), "/") of
        ["jobs", "client", ClientId, JobId] ->
            Header = [Client#client.id, " -- zadanie ", JobId,"\n=========================\n"],
            {atomic, L} = mnesia:transaction(fun() -> qlc:eval(qlc:q(
                                    [["* id: ", X#job.id, " status: ", state_to_str(X#job.status), "\n"]
                                     || X <- mnesia:table(job),
                                        X#job.id =:= JobId,
                                        X#job.client =:= ClientId
                                       ]
                                    )) end ),
            lists:flatten([Header|L]);
        ["jobs", "client", ClientId] ->
            Header = [Client#client.id, " -- lista zadan\n=========================\n"],
            {atomic, L} = mnesia:transaction(fun() -> qlc:eval(qlc:q(
                                    [["* id: ", X#job.id, " status: ", state_to_str(X#job.status), "\n"]
                                     || X <- mnesia:table(job),
                                        X#job.client =:= ClientId]
                                    )) end ),
            lists:flatten([Header|L])
    end,
    {Body, ReqData, Context}.


-spec post_is_create(term(), #ctx{}) -> {boolean(), term(), #ctx{}}.
post_is_create(ReqData, Context) ->
    {true, ReqData, Context}.


-spec create_path(term(), #ctx{}) -> {string(), term(), #ctx{}}.
%% @doc Gdy prawdziwy jest test post_is_create/2, to dla POST-ów wywoływana jest ta
%% metoda; generuje ona ścieżkę (URI) a potem wszystko odbywa się tak, jak z PUT-em.
create_path(ReqData, Context) ->
    Now = lists:flatten(string:join([integer_to_list(X) || X<-tuple_to_list(now())], "_")),
    Job = #job{id = Now,
               client = Context#ctx.client#client.id,
               status = waiting,
               nodes = []},
    {Now, ReqData, Context#ctx{job=Job}}.


-spec from_file(term(), #ctx{}) -> {boolean(), term(), #ctx{}}.
%% @doc Odbieranie danych (POST/PUT).
from_file(ReqData, #ctx{operation = cancel} = Context) ->
    JobId = wrq:path_info(jobid, ReqData),
    case mnesia:transaction(fun() ->
                    [J] = mnesia:wread({job, JobId}),
                    case J#job.status of 
                        disposed -> ok;
                        cancelled -> ok;
                        {error, _} -> ok;
                        waiting -> mnesia:write(J#job{status = cancelled});
                        _ -> mnesia:abort("Too late")
                    end
            end) of
        {atomic, _} -> {ok, ReqData, Context};
        {aborted, _} -> {{halt, 405}, ReqData, Context}
    end;
from_file(ReqData, #ctx{client = Client, job = Job, operation = getlist} = Context) ->
    %% http://wiki.basho.com/Webmachine-Streamed-Body.html
    %%   It [wrq:req_body/1] will provide the whole incoming request body as a binary.
    %%   (Unless the body is too large, as set by wrq:set_max_recv_body/2 or defaulting to 50M)
    Body = wrq:req_body(ReqData),
    Pkgname  = ["priv/pkgs/", Client#client.id, "__", Job#job.id, ".tgx"],
    Filename = ["priv/pkgs/", Client#client.id, "__", Job#job.id, ".tgx.gpg"],
    ok = file:write_file(Filename, Body),
    case verify_file(Filename, Pkgname) of
        {Lat,Lng}  -> 
            FilledJob = Job#job{dst_lat = Lat, dst_lng = Lng},
            mnesia:transaction(fun() -> mnesia:write(FilledJob) end),
            spawn(fun() -> send_pkg(FilledJob) end),
            {ok, ReqData, Context#ctx{job=FilledJob}};
        false -> {{halt, 406}, ReqData, Context}
    end.


-spec calculate({float(), float()}, {float(), float()}) -> float().
%% @doc Oblicza odległość współrzędnych geograficznych między sobą.
%% Wynikiem jest kąt w radianach.
calculate( {GpsThA, GpsPhA}, {GpsThB, GpsPhB}) ->
    Coef = pi()/180.0,
    {ThA, PhA} = {GpsThA*Coef, GpsPhA*Coef},
    {ThB, PhB} = {GpsThB*Coef, GpsPhB*Coef},
    {XA,YA,ZA} = {cos(ThA)*cos(PhA), cos(ThA)*sin(PhA), sin(ThA)},
    {XB,YB,ZB} = {cos(ThB)*cos(PhB), cos(ThB)*sin(PhB), sin(ThB)},
    case XA*XB+YA*YB+ZA*ZB of
        X when X > 1.0  -> acos(1.0);
        X when X < -1.0 -> acos(-1.0);
        X               -> acos(X)
    end.

-spec send_pkg(#job{}) -> term().
%% @doc Wysyła paczkę do node'a.
send_pkg(#job{dst_lat = Lat, dst_lng = Lng, nodes = Nodes, id = Id, client = Client} = Job) ->
    {atomic, Ns} = mnesia:transaction(fun() -> qlc:eval(qlc:q(
                            [{X, X#node.lat, X#node.lng} || X <- mnesia:table(node),
                                                            X#node.active =:= true,
                                                            not lists:any(fun(Y) -> Y =:= X#node.id end, Nodes)]
                            )) end),
    F = fun({X, Xlat, Xlng}) -> {calculate({Xlat,Xlng}, {Lat, Lng}), X} end,
    [{_,H}|_] = lists:keysort(1, lists:map(F, Ns)),
    Pkgname = [Client, "__", Id, ".tgx"],
    file:copy(["priv/pkgs/", Pkgname], ["/mnt/lipa/", H, "/", Pkgname]),
    mnesia:transaction(fun() -> mnesia:write(
                    Job#job{
                        status = waiting,
                        nodes = [H|Nodes]}) end).

-spec verify_file(iolist(), iolist()) -> boolean().
%% @doc Weryfikuje plik: czy dobrze zaszyfrowany, właściwie podpisany, czy zawiera
%% plik settings.conf który zawiera niezbędne informacje.
verify_file(Filename, Pkgname) ->
    os:cmd(["gpg --output \"", Pkgname, "\" --decrypt \"", Filename, "\""]),
    os:cmd(["rm -f \"", Filename, "\""]),
    Config = parse_ini(Pkgname),
    try begin
            {_, [_|_]       } = lists:keyfind({"Client", "Name"    }, 1, Config),
            {_, [_|_]       } = lists:keyfind({"Client", "Address" }, 1, Config),
            {_, [_|_] = Lat } = lists:keyfind({"Client", "Lat"     }, 1, Config),
            {_, [_|_] = Lng } = lists:keyfind({"Client", "Lng"     }, 1, Config),
            {list_to_float(Lat), list_to_float(Lng)}
        end
    catch
        _:_ -> false
    end.


-spec parse_ini(string()) -> [{{atom() | string(), string()}, string()}].
%% @doc Parsowanie plików ini. Na tyle małe i na tyle zgrabne, że aż głupio byłoby
%% sięgać po zewnętrzną bibliotekę.
parse_ini(Filename) ->
    Str = string:tokens(os:cmd(["tar JxOf \"", Filename, "\" settings.conf"]), "\n"),
    parse_ini(Str, default, []).

-spec parse_ini([string()], atom() | string(), [{{atom() | string(), string()}, string()}]) ->
                       [{{atom() | string(), string()}, string()}].
parse_ini([], _Group, Res) ->
    Res;
parse_ini([Head|Tail], Group, Res) ->
    [F|R] = Str = string:strip(Head),
    case F of
        $# -> parse_ini(Tail, Group, Res);
        $[ -> parse_ini(Tail, string:strip(R, both, $]), Res);
        _ ->
            [K|V] = string:tokens(Str, "="),
            parse_ini(Tail, Group, [{{Group, K}, string:join(V,"=")}|Res])
    end.
