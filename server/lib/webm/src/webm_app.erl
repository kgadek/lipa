%% @author author <author@example.com>
%% @copyright YYYY author.

%% @doc Callbacks for the webm application.

-module(webm_app).
-author('author <author@example.com>').

-behaviour(application).
-export([start/2,stop/1]).


%% @spec start(_Type, _StartArgs) -> ServerRet
%% @doc application start callback for webm.
start(_Type, _StartArgs) ->
    webm_sup:start_link().

%% @spec stop(_State) -> ServerRet
%% @doc application stop callback for webm.
stop(_State) ->
    ok.
