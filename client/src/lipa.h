#ifndef LIPA_H
#define LIPA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <glib.h>
#include <sys/stat.h>

#define WORKING_DIR "packages/"
#define UNPACKED_DIR "packages/unpacked/"
#define LOG_DIR "logs/"
#define EXTENSION "tgx"
#define SETTINGS_FILENAME "settings.conf"
/* user serveraddr file */
#define GPG_SIGN "gpg --sign --encrypt --recipient %s@%s %s"

/*
 * Aktualna struktura katalogow
 *
 * .
 * \__data
 * |
 * \__packages
 *    |
 *    \__unpacked
 *       |
 *       \_katalog_paczki
 *       |
 *       \ ...
 */

/* Rodzaj papieru */
enum paper_type_e
{
	HARD = 0,
	SOFT
};

/* Rozmiar papieru */
enum paper_size_e
{
	A0 = 0,
	A1,
	A2,
	A3,
	A4,
	A5,
	B5
};

/* W tej strukturze beda trzymane ustawienie do druku */
typedef struct {
	char * client_name;
	char * address;
	char * dir_path;
	unsigned int quantity;
	enum paper_type_e paper_type;
	enum paper_size_e paper_size;
} package_settings_t;

/* Wczytuje ustawienia drukowania */
int load_settings(const char * path, package_settings_t * settings)
{
	char * s_path = (char *) malloc(sizeof(char) * strlen(path) + strlen(SETTINGS_FILENAME));
	printf("dostalem sciezke: %s\n", path);
	strcpy(s_path, path);
	strcat(s_path, SETTINGS_FILENAME);
	printf("otwieram plik: %s\n", s_path);
	if(s_path == NULL)
	{
		perror("error while malloc");
		return EXIT_FAILURE;
	}
	
	/* glib file parser, bo i tak pisane pod gtk */ 
	GKeyFile * g_key = g_key_file_new();
	GKeyFileFlags flags = G_KEY_FILE_KEEP_COMMENTS;
	GError * error = NULL;

	if(!g_key_file_load_from_file(g_key, s_path, flags, &error))
	{
		g_error(error->message);
		return EXIT_FAILURE;
	}

	printf("parsuje plik konfiguracyjny\n");

	settings->client_name = g_key_file_get_string(g_key, "Client", "Name", NULL);
	settings->address = g_key_file_get_string(g_key, "Client", "Address", NULL);
	printf("%s\n%s\n", settings->client_name, settings->address);
	settings->quantity = g_key_file_get_integer(g_key, "Order", "Quantity", NULL);
	printf("ile?: %d\n", settings->quantity);
	settings->paper_type = (enum paper_type_e) g_key_file_get_integer(g_key, "Paper", "Type", NULL);
	printf("rodzaj papieru: %d\n", settings->paper_type);
	settings->paper_size = (enum paper_size_e) g_key_file_get_integer(g_key, "Paper", "Size", NULL);
	printf("rozmiar papieru: %d\n", settings->paper_size);

	settings->dir_path = (char *) malloc(sizeof(char) * strlen(path));
	if(settings->dir_path == NULL)
	{
		perror("error while malloc");
		return EXIT_FAILURE;
	}
	strcpy(settings->dir_path, path);

	g_key_file_free(g_key);

};

/* Wczytaj kolejke */
int load_queue(package_settings_t ** tab, int * size)
{
	DIR *d;
	struct dirent * dir;

	printf("otwieram katalog: %s\n", UNPACKED_DIR);
	if( (d = opendir(UNPACKED_DIR)) == NULL)
	{
		perror("error while opening dir");
		return EXIT_FAILURE;
	}


	(*size) = 0;
	while( (dir = readdir(d)) != NULL)
		if(dir->d_type == DT_DIR)
			(*size)++;
	closedir(d);

	if( (d = opendir(UNPACKED_DIR)) == NULL)
	{
		perror("error while opening dir");
		return EXIT_FAILURE;
	}
	

	(*tab) = (package_settings_t *) malloc(sizeof(package_settings_t) * (*size));
	if( (*tab) == NULL)
	{
		perror("error while malloc");
		return EXIT_FAILURE;
	}

	int i = 0;
	while( (dir = readdir(d)) != NULL)
	{
		if(dir->d_type == DT_DIR && strcmp(dir->d_name, ".") && strcmp(dir->d_name, ".."))
		{
			DIR * sub_dir;
			struct dirent * sub_dirent;
			char w_dir[strlen(UNPACKED_DIR) + strlen(dir->d_name) + 1];

			if( w_dir == NULL)
			{
				perror("error while malloc");
				return EXIT_FAILURE;				
			}
			strcpy(w_dir, UNPACKED_DIR);
			strcat(w_dir, dir->d_name);
			strcat(w_dir, "/");

			printf("otwieram katalog: %s\n", w_dir);
			if( (sub_dir = opendir(w_dir)) == NULL)
			{
				perror("error while opening dir");
				return EXIT_FAILURE;
			}

			// while
			while( (sub_dirent = readdir(sub_dir)) != NULL)
			{
				if(sub_dirent->d_type == DT_REG && !strcmp(sub_dirent->d_name, SETTINGS_FILENAME))
				{
					package_settings_t settings;
					load_settings(w_dir, &settings);
					(*tab)[i] = settings;
					i++;
				}
			}
			
			closedir(sub_dir);
		}
	}

	closedir(d);

	printf("koncze wczytywanie\n");
}

/* Bo GCC nie ma */
char * itoa(int val, int base)
{	
	static char buf[32] = {0};
			
	int i = 30;
	for(; val && i ; --i, val /= base)					
		buf[i] = "0123456789abcdef"[val % base];

	return &buf[i + 1];									
}

/* Zwraca nazwe kolejnej paczki */
int get_next_package(char ** f_name, int * size)
{
	DIR *d;
	struct dirent *dir;
	
	if( (d = opendir(WORKING_DIR)) == NULL)
	{
		perror("error while opening dir");
		return EXIT_FAILURE;
	}

	while( (dir = readdir(d)) != NULL)
	{
		if(dir->d_type == DT_REG)
		{
			size_t nl = strlen(dir->d_name), el = strlen(EXTENSION);
			if(nl >= el && !strcmp(dir->d_name + nl - el, EXTENSION))
			{
				(*size) = strlen(dir->d_name);
				printf("size=%d\n", (*size));
				(*f_name) = (char *) malloc(sizeof(char) * (*size));
				if( (*f_name) == NULL)
				{
					perror("error while malloc");
					return EXIT_FAILURE;
				}
				
				strncpy( (*f_name), dir->d_name, (*size));
				closedir(d);

				return EXIT_SUCCESS;
			}
		}
	}

	closedir(d);
	return EXIT_SUCCESS;
}

/* Rozpakowywuje paczke i usuwa ja */
int unpack(const char * tmp_f_name, const int size)
{
	char * f_name = (char *) malloc(sizeof(char) * (size + 1));
	strncpy(f_name, tmp_f_name, size);
	f_name[size] = '\0';

	time_t now;
	time(&now);

	char * str;
	str = itoa(now, 10);
	char * dir = (char *) malloc(sizeof(char) * 512);
	if(dir == NULL)
	{
		perror("error while malloc");
		free(f_name);
		return EXIT_FAILURE;
	}

	strcpy(dir, UNPACKED_DIR);
	strcat(dir, tmp_f_name);
	printf("dir=%s\n", tmp_f_name);

	if( mkdir( dir, S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IXUSR | S_IXGRP | S_IXOTH) < 0 )
	{
		perror("error while creating directory");
		free(f_name);
		free(dir);
		return EXIT_FAILURE;
	}

	char * cmd = (char *) malloc(sizeof(char) * 512);
	if(cmd == NULL)
	{
		perror("error while malloc");
		free(f_name);
		return EXIT_FAILURE;
	}
	strcpy(cmd, "tar -xJC ");
	strcat(cmd, dir);
	strcat(cmd, " -f ");
	strcat(cmd, WORKING_DIR);
	strcat(cmd, f_name);

	free(dir);
	
	printf("cmd=%s\n",cmd);

	if(system( cmd ) < 0)
	{
		perror("error while unpacking");
		free(f_name);
		return EXIT_FAILURE;
	}
	
	strcpy(cmd, WORKING_DIR);
	strcat(cmd, f_name);
	
	if( remove( cmd ) < 0)
	{
		perror("error while removing package");
		free(f_name);
		return EXIT_FAILURE;
	}

	free(f_name);

	return EXIT_SUCCESS;
}

/* Wlacza demona sshd. Niekoniecznie potrzebne */
int start_sshd()
{
	if( system("/usr/sbin/sshd -f /etc/ssh/sshd_config")  < 0)
	{
		perror("error while starting sshd");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

int print_pdfs(const char * d_name)
{
	DIR * d;
	struct dirent * dir;

	if( (d = opendir(d_name)) == NULL)
	{
		perror("error while opening directory");
		return EXIT_FAILURE;
	}

	while ( (dir = readdir(d)) != NULL)
	{
		if(dir->d_type == DT_REG)
		{
			size_t nl = strlen(dir->d_name), el = strlen("pdf");
			if(nl >= el && !strcmp(dir->d_name + nl - el, "pdf"))
			{
				char * cmd = (char *) malloc(sizeof(char) * 512);
				if(cmd == NULL)
				{
					perror("error while malloc");
					return EXIT_FAILURE;
				}
				strcpy(cmd, "lpr ");
				strcat(cmd, d_name);
				strcat(cmd, "/");
				strcat(cmd, dir->d_name);
				
				if( system(cmd) )
				{
					perror("error while queuing pdf");
					return EXIT_FAILURE;
				}
			}
		}
	}
}

int log_file(const char * filename, const char * msg, const char * user, const char * serveraddr, const char * pass)
{
	FILE * fp;
	printf("filename = %s\n");
	char * file = (char *) malloc(sizeof(char) * 512);
	strcpy(file, LOG_DIR);
	strcat(file, filename);
	strcat(file, ".log");
	
	printf("after string funcs\n");
	
	if( (fp = fopen("raport.tmp", "w")) == NULL)
	{
		perror("fopen failed");
		return EXIT_FAILURE;
	}
	
	fprintf(fp, msg);
	
	fclose(fp);
	printf("log file saved\n");
	
	printf("newfile: %s\n", file);
	
	if( rename("raport.tmp", file) < 0)
	{
		perror("error while moving file");
		free(file);
		return EXIT_FAILURE;
	}
	free(file);
	printf("log file renamed\n");
	
	char * cmd = (char *) malloc(sizeof(char) * 512);
	sprintf(cmd, GPG_SIGN, user, serveraddr, pass);

	system(cmd);
	free(cmd); 
	
	return EXIT_SUCCESS;
}

#endif
