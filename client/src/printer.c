#include "lipa.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <gtk/gtk.h>
#include <pthread.h>

#define UNPACK_INTERVAL 10
#define QUEUE_BUFFER 100

/* [node]:[pass] [serweraddr]/jobs/node/[nodeid]/[jobid] */
#define CURL_PRINT "curl -s --write-out \"%{http_code}\" -u %s:%s %s/jobs/node/%s/%s"
/* [node]:[pass] [serveraddr]/jobs/node/[nodeid]/[jobid]/printed [filename] */
#define CURL_RAPORT "curl -s --write-out \"%{http_code}\" -u %s:%s %s/jobs/node/%s/%s/printed --data-binary @%s"
/* [node]:[pass] [serveraddr]/jobs/node/[nodeid]/[jobid]/failed [filename] */
#define CURL_ERROR "curl -s --write-out \"%{http_code}\" -u %s:%s %s/jobs/node/%s/%s/failed --data-binary @%s"
/* [node]:[pass] [serveraddr] [nodeid] [jobid] */
#define CURL_OUTSOURCE "curl -s --write-out \"%{http_code}\" -u %s:%s %s/jobs/node/%s/%s/rejected"

#define OK_CODE 200
#define SERVER_DEAD 500

package_settings_t * queue = NULL;
GtkWidget * label[5];
GtkWidget * print_btn;
GtkWidget * error_btn;
GtkWidget * raport_btn;
GtkWidget * outsource_btn;

GtkWidget * window;
GtkTextBuffer * buffer;

size_t queue_size = 0;
char * current_dir = NULL;
char * node_name = NULL;
char * server_address = NULL;
char * node_pass = NULL;

enum
{
	COL_NAME = 0,
	COL_PATH,
	NUM_COLS
};

void * unpack_thread(void * unused)
{
	while(1)
	{
		char * next_package;
		int size = 0;
		get_next_package(&next_package, &size);
		if(size != 0)
		{
			printf("unpacking package\n");
			unpack(next_package, size);
		}

		sleep(UNPACK_INTERVAL);
	}
}

static GtkTreeModel * create_and_fill_model()
{
	GtkListStore * store;
	GtkTreeIter iter;

	store = gtk_list_store_new(NUM_COLS, G_TYPE_STRING, G_TYPE_STRING);

	int i = 0;
	for(i = 0 ; i < queue_size ; i++)
	{
		gtk_list_store_append(store, &iter);
		gtk_list_store_set(store, &iter,
			COL_NAME, queue[i].client_name,
			COL_PATH, queue[i].dir_path,
			-1);
	}

	return GTK_TREE_MODEL(store);
}

static GtkWidget * create_view_and_model()
{
	GtkCellRenderer * renderer;
	GtkTreeModel * model;
	GtkWidget * view;

	view = gtk_tree_view_new();

	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
		-1,
		"Client name",
		renderer,
		"text", COL_NAME,
		NULL);

	renderer = gtk_cell_renderer_text_new();
	gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
		-1,
		"Path",
		renderer,
		"text", COL_PATH,
		NULL);

	model = create_and_fill_model();
	gtk_tree_view_set_model(GTK_TREE_VIEW(view), model);
	g_object_unref(model);

	return view;

}

static gboolean delete_event(GtkWidget * window, GdkEvent * event, gpointer data)
{
	return TRUE;
}

static void destroy(GtkWidget * window, gpointer data)
{
	gtk_main_quit();
}

static void destroy_text(GtkWidget * w, gpointer data)
{
	gtk_widget_destroy(w);
}

static void unpack_callback(GtkWidget * widget, gpointer data)
{
	char * name = NULL;
	int size;
	get_next_package(&name, &size);
	
	unpack(name, size);
}

static void print_callback(GtkWidget * widget, gpointer data)
{
	char * jobid;
	jobid = strtok(current_dir, "/");
	jobid = strtok(NULL, "/");
	jobid = strtok(NULL, "/");
	
	char * cmd = (char *) malloc(sizeof(char) * 1024);
	sprintf(cmd, CURL_PRINT, node_name, node_pass, server_address, node_name, jobid);
	printf("curl cmd: %s\n", cmd);
	
	int return_code = 0;
	
	return_code = system(cmd);
	if(return_code != OK_CODE)
	{
		if(return_code != SERVER_DEAD)
		{
			show_error(window, "Printing not confirmed!");
			return;
		}
	}

	printf("PRINTING: %s\n", current_dir);
	if(print_pdfs(current_dir) == EXIT_FAILURE)
	{
		show_error(window, "Error while printing");
	}
	else
	{
		show_info(window, "Printing");
	}
}

static void log_raport_callback(GtkWidget * text_area, gpointer data)
{
	char * dir_tmp = (char *) malloc(sizeof(char) * strlen(current_dir));
	strcpy(dir_tmp, current_dir);
	char * jobid = NULL;
	jobid = strtok(dir_tmp, "/");
	jobid = strtok(NULL, "/");
	jobid = strtok(NULL, "/");
	
	printf("filename=%s\n", jobid);
	
	char * filename = (char *) malloc(sizeof(char) * 512);
	strcpy(filename, LOG_DIR);
	strcat(filename, jobid);
	strcat(filename, ".log");
	
	char * cmd = (char *) malloc(sizeof(char) * 1024);
	sprintf(cmd, CURL_RAPORT, node_name, node_pass, server_address, node_name, jobid, filename);
	printf("curl cmd: %s\n", cmd);

	GtkTextIter start;
  	GtkTextIter end;
  	gtk_text_buffer_get_bounds (buffer, &start, &end);
	char * msg = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
	printf("msg=%s\n", msg);
	
	if( log_file(jobid, msg, node_name, server_address, node_pass) < 0)
	{
		show_error(window, "Error while logging");
	}
	else
	{
		show_info(window, "Send");
	}
	
	system(cmd);
}

static void log_error_callback(GtkWidget * text_area, gpointer data)
{
	char * dir_tmp = (char *) malloc(sizeof(char) * strlen(current_dir));
	strcpy(dir_tmp, current_dir);
	char * jobid = NULL;
	jobid = strtok(dir_tmp, "/");
	jobid = strtok(NULL, "/");
	jobid = strtok(NULL, "/");
	
	printf("filename=%s\n", jobid);
	
	char * filename = (char *) malloc(sizeof(char) * 512);
	strcpy(filename, LOG_DIR);
	strcat(filename, jobid);
	strcat(filename, ".log");
	
	char * cmd = (char *) malloc(sizeof(char) * 1024);
	sprintf(cmd, CURL_ERROR, node_name, node_pass, server_address, node_name, jobid, filename);
	printf("curl cmd: %s\n", cmd);
	
	GtkTextIter start;
  	GtkTextIter end;
  	gtk_text_buffer_get_bounds (buffer, &start, &end);
	char * msg = gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
	printf("msg=%s\n", msg);
	
	if( log_file(jobid, msg, node_name, server_address, node_pass) < 0)
	{
		show_error(window, "Error while logging");
	}
	else
	{
		show_info(window, "Send");
	}
	
	system(cmd);
}


static void error_callback(GtkWidget * widget, gpointer data)
{
	GtkWidget * text_window;
	text_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(text_window), 400, 300);
	gtk_signal_connect((GtkObject *) text_window, "destroy", G_CALLBACK(destroy_text), NULL);
	
	GtkWidget * box1;
  	GtkWidget * button;
  	GtkWidget * text_area;
  	
  	box1 = gtk_vbox_new(TRUE, 0);
  	button = gtk_button_new_with_label("Send");
  	text_area = gtk_text_view_new();
  	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_area));
  	
  	gtk_text_set_editable(text_area, 1);
	gtk_text_set_word_wrap(text_area, 1);

  	gtk_container_add(GTK_CONTAINER(box1), text_area);  	
  	gtk_container_add(GTK_CONTAINER(box1), button);  	
  	gtk_container_add(GTK_CONTAINER(text_window), box1);
  	
  	g_signal_connect (button, "clicked", G_CALLBACK (log_error_callback), NULL);
	
	gtk_widget_show_all(text_window);
}

static void outsource_callback(GtkWidget * window, gpointer data)
{
	char * jobid;
	jobid = strtok(current_dir, "/");
	jobid = strtok(NULL, "/");
	jobid = strtok(NULL, "/");
	
	char * cmd = (char *) malloc(sizeof(char) * 1024);
	sprintf(cmd, CURL_OUTSOURCE, node_name, node_pass, server_address, node_name, jobid);
	printf("curl cmd: %s\n", cmd);
	
	int return_code = 0;
	
	return_code = system(cmd);
	if(return_code != OK_CODE)
	{
		show_error(window, "Outsource not confirmed!");
		return;
	}
	else
	{
		show_info(window, "Outsource called");
		if(remove(current_dir) < 0)
			perror("Removing outsourced directory failed! Please remove it manually");
		return;
	}
}

static void raport_callback(GtkWidget * widget, gpointer data)
{
	GtkWidget * text_window;
	text_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size(GTK_WINDOW(text_window), 400, 300);
	gtk_signal_connect((GtkObject *) text_window, "destroy", G_CALLBACK(destroy_text), NULL);
	
	GtkWidget * box1;
  	GtkWidget * button;
  	GtkWidget * text_area;
  	
  	box1 = gtk_vbox_new(FALSE, 0);
  	button = gtk_button_new_with_label("Send");
  	text_area = gtk_text_view_new();
  	buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(text_area));
  	
  	gtk_text_set_editable(text_area, 1);
	gtk_text_set_word_wrap(text_area, 1);

  	gtk_container_add(GTK_CONTAINER(box1), text_area);  	
  	gtk_box_pack_start(GTK_BOX(box1), button, FALSE, FALSE, 0);  	
  	gtk_container_add(GTK_CONTAINER(text_window), box1);
  	
  	g_signal_connect (button, "clicked", G_CALLBACK (log_raport_callback), NULL);
	
	gtk_widget_show_all(text_window);
}

void show_info(gpointer window, const char * dialog_message)
{
	if(dialog_message == NULL)
		return;
		
	GtkWidget *dialog;
	dialog = gtk_message_dialog_new(GTK_WINDOW(window),
            GTK_DIALOG_DESTROY_WITH_PARENT,
            GTK_MESSAGE_INFO,
            GTK_BUTTONS_OK,
            dialog_message, "Info");
	gtk_window_set_title(GTK_WINDOW(dialog), "Information");
	gtk_dialog_run(GTK_DIALOG(dialog));
	gtk_widget_destroy(dialog);
}

void show_error(gpointer window, const char * dialog_message)
{
  	GtkWidget *dialog;
  	dialog = gtk_message_dialog_new(GTK_WINDOW(window),
            GTK_DIALOG_DESTROY_WITH_PARENT,
            GTK_MESSAGE_ERROR,
            GTK_BUTTONS_OK,
            dialog_message);
  	gtk_window_set_title(GTK_WINDOW(dialog), "Error");
  	gtk_dialog_run(GTK_DIALOG(dialog));
  	gtk_widget_destroy(dialog);
} 

void show_details(GtkTreeView * treeview, GtkTreePath * path, GtkTreeViewColumn * column,
	gpointer userdata)
{
	gtk_widget_set_sensitive(print_btn, TRUE);
	gtk_widget_set_sensitive(error_btn, TRUE);
	gtk_widget_set_sensitive(raport_btn, TRUE);
	gtk_widget_set_sensitive(outsource_btn, TRUE);

	GtkTreeModel * model;
	GtkTreeIter iter;

	model = gtk_tree_view_get_model(treeview);

	if(gtk_tree_model_get_iter(model, &iter, path))
	{
		gchar * name;
		gtk_tree_model_get(model, &iter, COL_PATH, &name, -1);

		int i = 0 ;
		package_settings_t set;

		for(i = 0 ; i < queue_size ; i++)
		{
			if(!strcmp(name, queue[i].dir_path))
			{
				set = queue[i];
				break;
			}
		}
	
		current_dir = (char *) malloc(sizeof(char) * strlen(set.dir_path));
		strcpy(current_dir, set.dir_path);
	
		char * lbl = (char *) malloc(sizeof(char) * 250);
		strcpy(lbl, "Client name: ");
		strcat(lbl, set.client_name);

		gtk_label_set_text((GtkLabel *) label[0], lbl);
		strcpy(lbl, "Delivery address: ");
		strcat(lbl, set.address);
		gtk_label_set_text((GtkLabel *) label[1], lbl);

		strcpy(lbl, "Quantity: ");
		strcat(lbl, itoa(set.quantity, 10));
		gtk_label_set_text((GtkLabel *) label[2], lbl);

		strcpy(lbl, "Paper size: ");
		strcat(lbl, itoa(set.paper_size, 10));
		gtk_label_set_text((GtkLabel *) label[3], lbl);

		strcpy(lbl, "Paper type: ");
		strcat(lbl, itoa(set.paper_type, 10));
		gtk_label_set_text((GtkLabel *) label[4], lbl);


		g_free(name);		
		free(lbl);
	}
}

/*
 *
 *		MAIN
 *
 */
int main(int argc, char ** argv)
{
	if(argc < 4)
	{
		printf("usage: %s [server_address] [node_id] [node_pass]\n", argv[0]);
		return EXIT_SUCCESS;
	}
	
	node_name = argv[2];
	server_address = argv[1];
	node_pass = argv[3];
	
	printf("serwer %s nodeid %s\n", server_address, node_name);
	
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);

	pthread_t thread_id;
	pthread_create(&thread_id, &attr, &unpack_thread, NULL);
	

	load_queue(&queue, &queue_size);
	printf("queue[0]=%s\n", queue[0].client_name);
	if(queue == NULL)
		queue = (package_settings_t *) malloc(sizeof(package_settings_t) * QUEUE_BUFFER);

	GtkWidget * view;
	GtkWidget * label_box;
	GtkWidget * box1;
	GtkWidget * box2;
	GtkWidget * halign[5];
	
	gtk_init(&argc, &argv);

	label[0] = gtk_label_new("Client name: ");
	label[1] = gtk_label_new("Delivery address: ");
	label[2] = gtk_label_new("Quantity: ");
	label[3] = gtk_label_new("Paper size: ");
	label[4] = gtk_label_new("Paper type: ");
	
	print_btn = gtk_button_new_with_label("Print");
	error_btn = gtk_button_new_with_label("Error");
	raport_btn = gtk_button_new_with_label("Raport");
	outsource_btn = gtk_button_new_with_label("Outsource to diffrent node");
	
	gtk_widget_set_sensitive(print_btn, FALSE);
	gtk_widget_set_sensitive(error_btn, FALSE);
	gtk_widget_set_sensitive(raport_btn, FALSE);
	gtk_widget_set_sensitive(outsource_btn, FALSE);
	
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	box1 = gtk_hbox_new(TRUE, 0);
	box2 = gtk_hbox_new(FALSE, 0);
	label_box = gtk_vbox_new(FALSE, 0);
	view = create_view_and_model();

	gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);

	/* Callbacks */
	gtk_signal_connect((GtkObject *) window, "destroy", G_CALLBACK(destroy), NULL);
	gtk_signal_connect((GtkObject *) view, "row-activated", (GCallback) show_details, NULL);
	g_signal_connect (print_btn, "clicked", G_CALLBACK (print_callback), NULL);
	g_signal_connect (error_btn, "clicked", G_CALLBACK (error_callback), NULL);
	g_signal_connect (raport_btn, "clicked", G_CALLBACK (raport_callback), NULL);
	g_signal_connect (outsource_btn, "clicked", G_CALLBACK (outsource_callback), NULL);

	/* Label box */
	int i = 0;
	for(i = 0 ; i < 5 ; i++)
	{
		halign[i] = gtk_alignment_new(0, 0, 0, 1);
		gtk_container_add(GTK_CONTAINER(halign[i]), label[i]);
		gtk_container_add(GTK_CONTAINER(label_box), halign[i]);
	}
		
	gtk_box_pack_start(GTK_BOX(box2), print_btn, FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(box2), error_btn);
	gtk_container_add(GTK_CONTAINER(box2), raport_btn);
	gtk_container_add(GTK_CONTAINER(box2), outsource_btn);
	
	gtk_container_add(GTK_CONTAINER(label_box), box2);
	
	gtk_container_add(GTK_CONTAINER(window), box1);
	gtk_container_add(GTK_CONTAINER(box1), label_box);
	gtk_box_pack_start(GTK_BOX(box1), view, TRUE, TRUE, 0);

	gtk_widget_show_all(window);

	gtk_main();

	return EXIT_SUCCESS;
}
